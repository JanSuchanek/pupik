
# ziskej ID spusteneho kontejneru elasticsearch
es_container_id=$(docker container ls | grep elasticsearch | cut -d" " -f1)

# spust kibanu prilinkovanou na tento container
docker run --link $es_container_id:elasticsearch -p 5601:5601 docker.elastic.co/kibana/kibana:7.3.1



