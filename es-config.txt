Konfigurace elasticsearch
========================================================

Před přidáváním záznamů je třeba nakonfigurovat index spuštěním
./init-index.sh

POZOR: konfigurace smaže veškerá data!

Nakonfigurováno, aby ignorovalo diakritiku, uPPerCaSE a koncovky českých slov.
Použit anglický ES; kdy koncovky slov se ignorují jejich odmazáním
zdeňkem = zdeňk

namísto slovníkového převodu:
zdeňkova = zdeněk

Další užitečné příkazy pro experimenty:
=============================================================

# kontrola konfigurace
GET article/_settings
GET /article/_mapping

# přidání záznamu
POST /redmine/_doc/redmine_12345
{
  "id": 12345,
  "author": "Pepa novák",
  "subject": "Update podání z Plánu práce centrály červen-září za TO",
  "description": "Prosím o update plnění podání z Plánu práce centrály červen-září příslušných do TO.\r\nBude sloužit jako podklad pro prezentaci na jednání RV tento víkend.\r\n\r\nDíky"
}

# vyhledání všech záznamů
GET /redmine/_search

# vyhledávání dle klíčového slova
GET /redmine/_search  
{
  "query": {
    "match": {
      "description": "Petr"
    }
  }
}

