#! /usr/bin/python3

import urllib.request
import json
import requests
import func


def import_redmine_agenda(project, status, import_all):
    """ import_all   .. importuj vsechny issues (jinak jen otevrene) """

    base_url = 'https://redmine.pirati.cz/projects/%s/issues.json?tracker_id=%s' % (project, status)
    if import_all: 
        base_url += '&status_id=*'

    resp = func.get_json(base_url)
    all_issues = []
    if resp:
        original_count = resp['total_count']
        if original_count:
            offset= 0
            while offset < original_count: 
                resp = func.get_json(base_url + '&amp;limit=100&amp;offset=%s' % offset)
                offset +=100
                all_issues.extend(resp['issues'])

    elastic = list(map(lambda x: {"id": x["id"], 
        "source": "redmine", 
        "url": "https://redmine.pirati.cz/issues/%s" % x["id"], 
        "date": x["created_on"], 
        "title": x["author"]["name"] + " - " + x["subject"], 
        "snipplet": x["description"][:200],
        "fulltext": " ".join([x["author"]["name"], x["subject"], x["description"]])
        }, all_issues))

    print("Redmine %s - %s: importing %s records" % (project, status, len(elastic)))
    func.elastic_save(elastic, 'redmine', True)


def pump():
    # importuj vybrane agendy. TODO neumi ziskat automaticky seznam vsech agend: projit redmine API
    for project in ('ao', 'kancelar-strany', 'kk', 'medialni-odbor', 'po', 'pravni-tym', 'rp', 'republikovy-vybor', 'to', 'zo'):
        for status in (12, 2, 13, 14, 15, 20):
            import_redmine_agenda(project, status, False)


if __name__=='__main__':
    pump()
