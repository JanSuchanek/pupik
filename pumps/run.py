#! /usr/bin/python3.4

"""	
    Periodicky spousti v ruznych intervalech jednotlive datove pumpy.
    De facto primitivni nahrada cronu; spousti se pri startu dockeru
    a bezi donekonecna dokud Ctrl+C
"""
      
import time

# pumpy
import forum
import proplaceni
import redmine    
    
if __name__=='__main__':

    # Nekonecna smycka, tempem 1x za minutu (+cas pro kod)
    iteration = 0
    while True:

        # pri kazdem opakovani  
        print("Every minute jobs:")
        forum.pump()

        if iteration%60 == 0: # 1x za hodinu
            print("Every hour jobs:")
            proplaceni.pump_zadosti()

        if iteration%240 == 0: # 1x za 4 hodiny
            print("Every 4 hour jobs:")
            redmine.pump()
            proplaceni.pump_zamery()

        iteration = iteration + 1 if iteration<1000000 else 0   
        
        # cekej minutu a zobrazuj counter
        sleep_length = 60
        for sleepiter in range(sleep_length):
            print("Sleeping for %s sec...  \r" % (sleep_length-sleepiter), end = '\r')
            time.sleep(1)
        print()
            
            