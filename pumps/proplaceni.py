#! /usr/bin/python3

""" datova pumpa pro proplaceni 
API:

# zadosti o proplaceni: obsahuje vsechny texty
https://piroplaceni.pirati.cz/rest/realItem/?format=json  # vsechny, v soucasnosti cca 7k polozek, cca 20 sec

# tyto tri by mely stacit pri prubeznem nacitani, aby pokryly vsechny itemy.
https://piroplaceni.pirati.cz/rest/realItem/?format=json&amp;state=1    # rozprac
https://piroplaceni.pirati.cz/rest/realItem/?format=json&amp;state=2    # ke schvaleni hospodarem
https://piroplaceni.pirati.cz/rest/realItem/?format=json&amp;state=3    # schvalena hospodarem

# zamery: obsahuje vsechny texty
https://piroplaceni.pirati.cz/rest/intent/?format=json      # vse, cca 1000 zameru, 5-10 sec

"""

import urllib.request
import json
import requests
import re
import func

IMPORT_ALL = False   # zda importovat vsechny zaznamy (pomale), nebo jen nejnovejsi (predpoklada pravidelne spousteni)
VERBOSE = True


def import_realitems(url):

    elastic = list(map(lambda x: {"id": x["id"], 
        "source": "proplaceni", 
        "url": "https://piroplaceni.pirati.cz/zadost/%s/" % x["id"], 
        "date": x["updatedStamp"], 
        "title": "Žádost " + x["name"], 
        "snipplet": x["prijemceNazev"] + " " + x["name"],
        "fulltext": " ".join(["Žádost", x["prijemceNazev"], x["name"]])
        }, func.get_json(url, VERBOSE)))
        
    func.elastic_save(elastic, 'proplaceni', VERBOSE)


def pump_zadosti():

    # realItems
    if IMPORT_ALL:
        import_realitems('https://piroplaceni.pirati.cz/rest/realItem/?format=json')
    else:
        import_realitems('https://piroplaceni.pirati.cz/rest/realItem/?format=json&amp;state=1')
        import_realitems('https://piroplaceni.pirati.cz/rest/realItem/?format=json&amp;state=2')
        import_realitems('https://piroplaceni.pirati.cz/rest/realItem/?format=json&amp;state=3')


def pump_zamery():

    def intent_id(url):
        """ vyextrahuj z URL zameru jeho cislo """
        return re.findall(r'/([0-9]*)/\?format',url)[0]

    # zamery
    elastic = list(map(lambda x: {"id": intent_id(x["url"]), 
        "source": "proplaceni", 
        "url": "https://piroplaceni.pirati.cz/zamer/%s/" % intent_id(x["url"]), 
        "date": x["appr_by_user_stamp"], 
        "title": "Záměr " + x["name"], 
        "snipplet": x["desc"][:200],
        "fulltext": " ".join(["Záměr", x["name"], x["desc"]])
        }, func.get_json('https://piroplaceni.pirati.cz/rest/intent/?format=json', VERBOSE)))

    func.elastic_save(elastic, 'proplaceni', VERBOSE)


if __name__=='__main__':
    pump_zadosti()
    pump_zamery()
        