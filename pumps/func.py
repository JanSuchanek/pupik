#! /usr/bin/python3

""" funkce pro datove pumpy """

import urllib.request
import json
import requests
import os

def get_json(url, verbose=False):
    """ Vrat JSON odpoved z adresy URL nebo none v pripade chyby """

    if verbose: print(url)
    req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla'})
    try:
        r = urllib.request.urlopen(req).read()
        return json.loads(r.decode('utf-8'))    
    except Exception as e:
        return None


def elastic_save(items, id_prefix, verbose=False):
    """ vlozi vsechny polozky v poli items do elasticsearch.
        id_prefix .. predpona unikatniho id v ES
        
        pripojuje se na adresu definovanou v promenne prostredi "PUMPS_ES_ADDRESS", neni-li, pak na 'localhost'
    """

    if verbose: 
        print("Pocet polozek: %s" % len(items))    
        
    address = os.getenv('PUMPS_ES_ADDRESS', 'localhost')    

    for item in items:
        url = 'http://%s:9200/article/_doc/%s_%s' % (address, id_prefix, item['id'])
        try:
            r = requests.post(url, json=item)    
        except requests.exceptions.ConnectionError:
            print("Cannot connect to ElasticSearch %s" % url)
            return 
        if not r.status_code in (200, 201):
            print("".join(["Error", str(r.status_code), r.text]))

