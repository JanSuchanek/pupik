#! /usr/bin/python3

""" datova pumpa pro evidenci datovych zameru

TODO: rozpracovano. je spravne naformatovana QUERY, ale stazeni nefunguje, vraci 403

API rozhrani na https://evidence-api.pirati.cz/graphql
format GraphQL https://graphql.org
"""

QUERY = """
{
  searchReports(query: "", first:100) {
    edges {
      node {
        date,
        title,
        receivedBenefit,
        providedBenefit,
        ourParticipants,
        otherParticipants,
        body,
        author {
          firstName,
          lastName
        }
      }
    }
  }
}

"""


import requests


def run_query(): # A simple function to use requests.post to make the API call. Note the json= section.
    request = requests.post('https://evidence-api.pirati.cz', json={'query': QUERY}, headers={'User-Agent': 'Mozilla'})
    if request.status_code == 200:
        return request.json()
    else:
        raise Exception("Query failed to run by returning code of {}. ".format(request.status_code))
        

if __name__=='__main__':

    resp = run_query()  # vraci 403
    print(resp)
    exit()
    
